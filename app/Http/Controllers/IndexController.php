<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mattbrown\Laracurl\Facades\Laracurl;

class IndexController extends Controller
{
    public $api_key = 'tplLLakU58TB72osCZuF1-FR';
    public function index()
    {
        $projects = json_decode(Laracurl::get('https://www.parsehub.com/api/v2/projects?api_key='. $this->api_key, true));
        return view('project-list', compact('projects'));
    }

    public function results(Request $request)
    {
        $token = $request->run_token;
        $url = 'https://www.parsehub.com/api/v2/runs/' . $token . '/data?api_key='. $this->api_key;
        $results = json_decode(gzdecode(Laracurl::get($url)), true);

        /**
         * in the array fetched from parsehub selection2 of index 14 was missing
         * hence removing index 14 from the array
         */

        foreach($results['title'] as $key => $result)
        {
            if (!array_key_exists('selection2', $result)) {
                unset($results['title'][$key]);
            }
        }

        /**
         * now that we have removed an index from the array we should
         * reindex it using array_values function
         */
        $titles = array_values($results['title']);

        /**
         * because we have price and title of phones
         * in a separate array we should unset these properties
         * from results array
         */

        unset($results['title']);

        /**
         * send these arrays namely results and titles to our view
         */
        return view('project-results', compact('results', 'titles'));
    }
}
