@extends('base')
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th>Showing 50 out of {{$results['total_listing'][0]['extract1']}}</th>
        </tr>
        <tr>
            <th>Title</th>
            <th>Price</th>
        </tr>
        </thead>

        <tbody>

        @foreach($titles as $result)
            <tr>
                <td>{{$result['extract2']}}</td>
                <td>{{$result['selection2']}}</td>
            </tr>
        @endforeach</tbody>
        </tbody>
    </table>
@stop

