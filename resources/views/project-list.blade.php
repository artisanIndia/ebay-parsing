@extends('base')
@section('content')
<table class="table">
    <thead>
    <tr>
        <th>Project Name</th>
        <th>Pages</th>
        <th>End time</th>
        <th>Parsed URL</th>
        <th>Results</th>
    </tr>
    </thead>

    <tbody>
    @foreach($projects as $project)
        @foreach($project as $singleproject)
    <tr>
        <td>{{$singleproject->title}}</td>
        <td>{{$singleproject->last_run->pages}}</td>
        <td>{{$singleproject->last_run->end_time}}</td>
        <td><a href="{{$singleproject->last_run->start_url}}" target="_blank">Click here</a></td>
        <td><a href="/project-result?run_token={{$singleproject->last_run->run_token}}">Show results</a></td>
    </tr>
        @endforeach
        @endforeach
    </tbody>
</table>
    @stop
