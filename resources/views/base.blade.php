<!DOCTYPE html>
<html>
<head>
    <title>Ebay content extraction</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="navbar navbar-default">
    <div class="navbar-brand">Ebay Data Scraping</div>
</div>
@yield('content')
</body>
</html>